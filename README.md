# Documentation technique

Pour faciliter l'intégration du SDK dans votre site, des options supplémentaires peuvent être utilisées.

## État de l'analyse

`onAnalysisStateUpdated: (isLoading: boolean) => void`

La callback est appelée à chaque fois qu'une analyse démarre ou se stoppe.

## Nombre d'alertes

`onAlertsCountUpdated: (alertsCount: number) => void`

Dès que le nombre d'alertes dans le texte de l'utilisateur est modifié, cette callback est appelée avec le nombre actualisé.

## État du panneau d'alertes

`onAlertsPanelStateUpdated: (isOpen: boolean) => void`

La callback est appelée dès que le panneau d'alertes est ouvert ou fermé.

## Placement du panneau d'alertes

`alertsPanelContainerSelector: string`

Permet de positionner le panneau d'alertes au sein d'un élément spécifique du DOM. Si plusieurs occurrences du sélecteur sont trouvées, la première est utilisée.

## Restreindre l'analyse à une langue spécifique

`allowedLanguage?: 'fr' | 'en' | 'de' | 'it' | 'es' | 'pt'`

Permet de restreindre l'analyse à une seule langue. L'option `enableMultilanguage` doit être activée si vous optez pour autre chose que la langue française.

## Évènements d'une session de correction

`onNewSessionEvent?: (eventType: SessionEventType, eventAttributes: any) => void`

La callback est appelée dès qu'un évènement majeur survient pendant la session de correction. Le paramètre `eventType` indique le type d'évènement qui vient de se produire, et le paramètre `eventAttributes` apporte des informations supplémentaires lorsque c'est nécessaire.

### Nouvelle correction

Dès que l'utilisateur applique une suggestion, un évènement `suggestion-applied` est déclenchée.

Le second paramètre fournit la catégorie, le code et la langue de la correction appliquée.

### Mise à jour de la liste des alertes

Dès que la liste des alertes est mise à jour, un évènement `alerts-list-updated` est déclenché, avec la nouvelle liste des alertes. Il est recommandé de faire un _diff_ entre la précédente valeur et la nouvelle pour limiter le flickering, car cet évènement peut être déclenché très fréquemment.

### Nouvelle autocorrection

Dès qu'une autocorrection est réalisée par le système, un évènement `autocorrection-added` est déclenché.

Le second paramètre fournit le nombre total d'autocorrections présentes dans le texte.

### Autocorrection "annulée"

Dès qu'une autocorrection est annulée par l'utilisateur, un évènement `autocorrection-undone` est déclenché.

Le second paramètre fournit le nombre total d'autocorrections présentes dans le texte.

### Reformulation

Dès qu'une reformulation est effectuée, un évènement `rewrite-suggestion-applied` est déclenchée.

Le second paramètre fournit le type de reformulation effectué : `sustained`, `fluent`, `synthetic`, `user-defined` (instructions personnalisées par l'utilisateur).

*Attention, les types de reformulation vont prochainement évoluer ; les responsables d'espaces MerciApp vont notamment pouvoir personnaliser les options proposées par défaut aux membres de leur entreprise.*